Word Inspector - 文档审查工具

起源
---------------
项目里面经常需要和Word文档打交道，比如解析、替换、生成等等操作，这些操作都要求程序员对Word文档的结构有着比较强的理解能力。在实际工作里面发现，网上缺乏对Word文档结构解析工具。所以借鉴了Web的Inspector，写了这个Word文档审查工具，方便程序员更加深入了解Word文档结构，不再浑水摸鱼。

![](http://7u2hwk.com1.z0.glb.clouddn.com/word-inspector-screenshot.png)

说明
---------------
1. 工具使用的是Aspose.Word，解析出来的结构和POI等工具解析出来的应该是相同的，所以使用其他Word库也可以参考本项目的代码。
2. Aspose.Word仅供学习使用，如需商用请购买正版。


GETTING STARTED
---------------
工具是用Java写的，所以运行环境要求安装JRE6+，dist目录里面有已经编译后的jar，直接可以运行。




