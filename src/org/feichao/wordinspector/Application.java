package org.feichao.wordinspector;

import java.io.File;

/**
 *
 * @author chao
 * @since 2015-4-28
 */
public class Application {

    public static String getName(){
        return "Word文档审查器";
    }

    public static File getTmpDir(){
        return new File("./tmp/");
    }

}
