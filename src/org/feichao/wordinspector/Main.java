/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.feichao.wordinspector;

import javax.swing.UIManager;

/**
 *
 * @author chao
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //设置风格
         try {
            String lookAndFeel = UIManager.getSystemLookAndFeelClassName();
            UIManager.setLookAndFeel(lookAndFeel);
        } catch (Exception e) {}

        System.setProperty("awt.useSystemAAFontSettings", "on");  
        System.setProperty("swing.aatext", "true");  

        MainFrame mainFrame = new MainFrame();
        //居中
        mainFrame.setLocationRelativeTo(null);
        //GOGO
        mainFrame.setVisible(true);
    }

}
