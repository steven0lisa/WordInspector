package org.feichao.wordinspector;

import com.aspose.words.Body;
import com.aspose.words.CompositeNode;
import com.aspose.words.Document;
import com.aspose.words.Node;
import com.aspose.words.NodeType;
import com.aspose.words.OleFormat;
import com.aspose.words.Paragraph;
import com.aspose.words.Run;
import com.aspose.words.Section;
import com.aspose.words.Shape;
import com.aspose.words.ShapeType;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.feichao.wordinspector.beans.DocumentNode;
import org.feichao.wordinspector.util.Logger;
import org.feichao.wordinspector.util.ValueNameUtil;

/**
 *
 * @author chao
 * @since 2015-1-29
 */
public class Model {

    MainFrame mainFrame;

    public Model(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
    }

    public List<DocumentNode> processFile(File file) {
        List<DocumentNode> documentNodes = new ArrayList<DocumentNode>();
        try {
            Document document = new Document(file.getAbsolutePath());
            for (Section section : document.getSections().toArray()) {
                DocumentNode sectionNode = new DocumentNode();
                sectionNode.type = ValueNameUtil.toStringWithoutValue(NodeType.class, section.getNodeType());
                sectionNode.content = section.getText();
                sectionNode.node = section;
                documentNodes.add(sectionNode);
                Body body = section.getBody();
                for (Node node : body.getChildNodes().toArray()) {
                    processNode(sectionNode, node);
                }
            }
        } catch (Exception e) {
            Logger.error(e, "Failed to parse document");
        }

        return documentNodes;
    }

    private void processNode(DocumentNode documentNode, Node node) {
        DocumentNode currDocumentNode = new DocumentNode();
        currDocumentNode.node = node;

       if (node.getNodeType() == NodeType.SHAPE) {
            Shape shape = (Shape) node;
            OleFormat format = shape.getOleFormat();
            if (format != null) {
                currDocumentNode.type = "SHAPE[OLE]";
                currDocumentNode.content = node.getText();
                documentNode.children.add(currDocumentNode);
            } else if (shape.getShapeType() == ShapeType.IMAGE) {
                currDocumentNode.type = "SHAPE[Image]";
                currDocumentNode.content = node.getText();
                documentNode.children.add(currDocumentNode);
            }
        } else {
            currDocumentNode.type = ValueNameUtil.toStringWithoutValue(NodeType.class, node.getNodeType());
            currDocumentNode.content = node.getText();
            documentNode.children.add(currDocumentNode);

            if (node instanceof CompositeNode) {
                CompositeNode cn = (CompositeNode) node;
                for (Node subNode : cn.getChildNodes().toArray()) {
                    processNode(currDocumentNode, subNode);
                }
            }
        }
    }
}
