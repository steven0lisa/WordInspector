package org.feichao.wordinspector.beans;

import com.aspose.words.DropCapPosition;
import com.aspose.words.Font;
import com.aspose.words.LineSpacingRule;
import com.aspose.words.Node;
import com.aspose.words.OutlineLevel;
import com.aspose.words.Paragraph;
import com.aspose.words.ParagraphAlignment;
import com.aspose.words.ParagraphFormat;
import com.aspose.words.Run;
import com.aspose.words.Section;
import com.aspose.words.StyleIdentifier;
import com.aspose.words.TextEffect;
import com.aspose.words.Underline;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.feichao.wordinspector.util.ValueNameUtil;

/**
 *
 * @author chao
 * @since 2015-1-29
 */
public class DocumentNode {

    public String type;

    public String content;

    public Node node;

    public List<DocumentNode> children = new ArrayList<DocumentNode>();

    public Map<String, Object> getAttributeMap(){
        Map<String, Object> map = new TreeMap<String, Object>();
        if (node instanceof Paragraph) {
            Paragraph a = (Paragraph) node;
            ParagraphFormat format = a.getParagraphFormat();
            map.put("ParagraphAlignment", ValueNameUtil.toString(ParagraphAlignment.class, format.getAlignment()));
            map.put("LineSpacingRule", ValueNameUtil.toString(LineSpacingRule.class, format.getLineSpacingRule()));
            map.put("OutlineLevel", ValueNameUtil.toString(OutlineLevel.class, format.getOutlineLevel()));
            map.put("StyleIdentifier", ValueNameUtil.toString(StyleIdentifier.class, format.getStyleIdentifier()));
            map.put("DropCapPosition", ValueNameUtil.toString(DropCapPosition.class, format.getDropCapPosition()));
            map.put("FirstLineIndent", format.getFirstLineIndent());

        }else if(node instanceof Run){
            Run a = (Run) node;
            Font font = a.getFont();
            map.put("TextEffect", ValueNameUtil.toString(TextEffect.class, font.getTextEffect()));
            map.put("Underline", ValueNameUtil.toString(Underline.class, font.getUnderline()));
            map.put("StyleIdentifier", ValueNameUtil.toString(StyleIdentifier.class, font.getStyleIdentifier()));
            map.put("FontName", font.getName());
            map.put("FontSize", font.getSize());
            map.put("FontColor", font.getColor());
            map.put("Bold", font.getBold());
            map.put("Underline", font.getUnderline());
            map.put("Italic", font.getItalic());
            map.put("Outline", font.getOutline());

        }else if(node instanceof Section){
            Section a = (Section) node;

        }
        return map;
    }

    @Override
    public String toString() {
        return type;
    }
}
