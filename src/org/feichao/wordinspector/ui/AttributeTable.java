/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.feichao.wordinspector.ui;

import com.aspose.words.DropCapPosition;
import com.aspose.words.Font;
import com.aspose.words.LineSpacingRule;
import com.aspose.words.Node;
import com.aspose.words.NodeType;
import com.aspose.words.OutlineLevel;
import com.aspose.words.Paragraph;
import com.aspose.words.ParagraphAlignment;
import com.aspose.words.ParagraphFormat;
import com.aspose.words.Run;
import com.aspose.words.Section;
import com.aspose.words.StyleIdentifier;
import com.aspose.words.TextEffect;
import com.aspose.words.Underline;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import org.feichao.wordinspector.beans.DocumentNode;
import org.feichao.wordinspector.util.ValueNameUtil;

/**
 *
 * @author chao
 */
public class AttributeTable extends JTable {

    {
        DefaultTableModel model = new DefaultTableModel(new Object[][]{
                }, new Object[]{"属性", "数值"});
        setModel(model);
    }

    public AttributeTable(Object[][] os, Object[] os1) {
    }

    public AttributeTable(Vector vector, Vector vector1) {
    }

    public AttributeTable(int i, int i1) {
    }

    public AttributeTable(TableModel tm, TableColumnModel tcm, ListSelectionModel lsm) {
    }

    public AttributeTable(TableModel tm, TableColumnModel tcm) {
    }

    public AttributeTable(TableModel tm) {
    }

    public AttributeTable() {
    }
    DocumentNode node;

    public void setDocumentNode(DocumentNode node) {
        this.node = node;
        update();
    }

    public void update() {
        //TODO
        Node theNode = node.node;
        DefaultTableModel model = new DefaultTableModel(new Object[][]{
                    {"NodeType", ValueNameUtil.toString(NodeType.class, theNode.getNodeType())}
                }, new Object[]{"属性", "数值"});

        Map<String, Object> attrs = node.getAttributeMap();
        Set<Map.Entry<String, Object>> set = attrs.entrySet();
        for(Map.Entry<String, Object> entry: set){
            model.addRow(new Object[]{
                entry.getKey(), entry.getValue()
            });
        }


        setModel(model);
    }
}
