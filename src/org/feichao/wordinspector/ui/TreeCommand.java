/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.feichao.wordinspector.ui;

/**
 *
 * @author chao
 */
public abstract class TreeCommand {

    public String text;

    public Object data;

    public abstract void doCommand();

    @Override
    public String toString(){
        return text;
    }

}
