package org.feichao.wordinspector.util;

/**
 *
 * @author chao
 * @since 2015-4-28
 */
public class Logger {

    public static void info(String msg, Object... args){
        System.out.printf(msg, args);
        System.out.println();
    }

    public static void error(Throwable e, String msg){
        System.err.println(msg);
        e.printStackTrace();
    }

}
