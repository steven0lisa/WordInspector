package org.feichao.wordinspector.util;

import java.lang.reflect.Field;

/**
 *
 * @author chao
 * @since 2015-4-28
 */
public class ValueNameUtil {

    static String toString(Class clazz, int value, boolean displayValue){
        Field[] fields = clazz.getFields();
        for(Field field: fields){
            try{
                if(field.getType() == int.class){
                    int fieldVal = field.getInt(null);
                    if(fieldVal == value){
                        if(displayValue){
                            return field.getName() + String.format(" (%s)", value);
                        }else{
                            return field.getName();
                        }
                        
                    }
                }
            }catch(Exception e){
                Logger.error(e, "Failed to read value");
            }
        }
        if(displayValue){
            return "UNKNOW" + String.format(" (%s)", value);
        }else{
            return "UNKNOW";
        }
        
    }

    public static String toString(Class clazz, int value){
        return toString(clazz, value, true);
    }

    public static String toStringWithoutValue(Class clazz, int value){
        return toString(clazz, value, false);
    }

}
